/*
*   iLight - Arduino Version
*   v1
*
*   Created by Ricky Dall'Armellina
*   Copyright © 2017 Ricky Dall'Armellina. All rights reserved.
*
*   Color File - color.h
*
*/

// MODE LIST
#define MAX_MODES       4
#define BLACKOUT_MODE   0
#define WHITEOUT_MODE   1
#define STATIC_MODE     2
#define RAINBOW_MODE    3

// LIGHT VARIABLES
#define SATURATION_CONST    100 //change this to change the saturation value of the lights
//#define SPEED_INC           0.1f
//#define SPEED_ADJUST        1
//unsigned int brightness = 75;
//float speed = 0.5;
int stdHue = 0; //used for rainbow mode, there has to be a better way to do it

// HSL structure
typedef struct HSL {
    int h, s, l;
} HSL;

// FUNCTION PROTOTYPES
void hsl_to_rgb();
double h_analysis(double p, double q, double t);

//need to pass array for h, s, l
void blackout(int hVal, int sVal, int lVal);
void whiteout(int hVal, int sVal, int lVal);
void static_color(int hVal, int sVal, int lVal);
void rainbow(int hVal, int sVal, int lVal);

HSL hsl;
int rgb[3] = { 0, 0, 0 };

void (*color_modes[MAX_MODES])(int hVal, int sVal, int lVal) = { blackout, whiteout, static_color, rainbow };

// COLOR MODES
void blackout(int hVal, int sVal, int lVal) {
    hsl.h = 0;
    hsl.s = 0;
    hsl.l = 0;
    LOG("Blackout\n");
}

void whiteout(int hVal, int sVal, int lVal) {
    hsl.h = 0;
    hsl.s = 0;
    hsl.l = lVal;
    LOG("Whiteout\n");
}

void static_color(int hVal, int sVal, int lVal) {
    hsl.h = hVal; //hue value input range 0-360
    hsl.s = sVal;
    hsl.l = lVal;
    LOG("Static Color Mode");
}

void rainbow(int hVal, int sVal, int lVal) {
    stdHue += 1;
    hsl.h = stdHue % 3600;
    hsl.s = SATURATION_CONST;
    hsl.l = lVal;
    LOG("Rainbow Color Mode\n");
}

// CONVERSION FUNCTIONS
void hsl_to_rgb() {
    double h = hsl.h / 360.0f;
    double s = hsl.s / 100.0f;
    double l = hsl.l / 100.0f;
    
    double r, g, b;
    
    if (s == 0) {
        r = g = b = l; // no color, just white
    }
    else {
        double q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        double p = 2 * l - q;
        r = h_analysis(p, q, (h + 1 / 3.0));
        g = h_analysis(p, q, h);
        b = h_analysis(p, q, (h - 1 / 3.0));
    }
    
    rgb[0] = r * 255;
    rgb[1] = g * 255;
    rgb[2] = b * 255;
    
    
    LOG("HSL->RGB color: ");
    LOG(rgb[0]);
    LOG(" ");
    LOG(rgb[1]);
    LOG(" ");
    LOG(rgb[2]);
    LOG("\n");
    
    
}

double h_analysis(double p, double q, double t) {
    if (t < 0) t += 1;
    if (t > 1) t -= 1;
    if (t < 1 / 6.0) return (p + (q - p) * 6 * t);
    if (t < 1 / 2.0) return q;
    if (t < 2 / 3.0) return (p + (q - p) * (2 / 3.0 - t) * 6);
    return p;
}